function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.

    //Conditons:
        // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
        // If letter is invalid, return undefined.
    if(letter.length!=1){
        return undefined
    }else{
        let arr = sentence.split("")
        arr.forEach(x=>{
            if(x==letter){
                result++
            }
        })
        return result
    }
}


function isIsogram(text) {
    //Goal:
        // An isogram is a word where there are no repeating letters.

    //Check:
        // The function should disregard text casing before doing anything else.
        text = text.toLowerCase();
        let arr = text.split('')
    //Condition:
        // If the function finds a repeating letter, return false. Otherwise, return true.
       for(let x =0; x<arr.length-1;x++){
        if(arr[x]==" "){
            continue
        }
            for(let y =x+1;y<arr.length-1;y++){
                if(arr[x]==arr[y]){
                    return false
                }
            }
       }
       return true
}

function purchase(age, price) {

    //Conditions:
        // Return undefined for people aged below 13.
        // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
        // Return the rounded off price for people aged 22 to 64.

    //Check:
        // The returned value should be a string.
    
        if(age<13){
            return undefined
        }else if(age<22){
            let discount = price*0.2;
            price = price-discount.toFixed(3).toString()
            if(parseInt(price[price.length-1]>=5)){
                price = Number(price)+0.01
                return price.toString()
            }else{
                return price.toFixed(2).toString()
            }
            
        }else if(age<65){

            price = price.toFixed(2).toString()
            return price
        }else{
            let discount = price*0.2;
            price = price-discount.toFixed(3).toString()
            if(parseInt(price[price.length-1])>5){
                price = Number(price)+0.01
                return price.toString()
            }else{
                return price.toFixed(2).toString()
            }
        }

}

function findHotCategories(items) {
    //Goal:
        // Find categories that has no more stocks.
        // The hot categories must be unique; no repeating categories.

    //Array for the test:
        // The passed items array from the test are the following:
        // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
        // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
        // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
        // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
        // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }


    //Expected return must be array:
        // The expected output after processing the items array is ['toiletries', 'gadgets'].
    let arr = [];
    // Note:
        // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
    items.forEach(obj=>{
        if(obj.stocks==0){
            if(!arr.includes(obj.category)){
                arr.push(obj.category)
            }
        }
    })
    return arr
}

function findFlyingVoters(candidateA, candidateB) {
    //Goal:
        // Find voters who voted for both candidate A and candidate B.

    //Array for the test:    
        // The passed values from the test are the following:
        // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
        // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // Expected return must be array:
        // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].

    //Note:
        // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    return candidateA.filter(x=>candidateB.includes(x))

}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};